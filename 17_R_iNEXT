####################
configfile: 'config.yaml'

####################
from snakemake.utils import R

####################
rule all:
    input:
        DataInfo = expand('out/{data}/{primer}/inext/{filt}-{q}/DataInfo.tsv',
                          data = config['datasets']['data'],  
                          primer = config['datasets']['primer'],
                          filt = config['datasets']['filter']['both'],
                          q = config['iNEXT']['q']),

        iNextEst = expand('out/{data}/{primer}/inext/{filt}-{q}/iNextEst.tsv',
                          data = config['datasets']['data'],  
                          primer = config['datasets']['primer'],
                          filt = config['datasets']['filter']['both'],
                          q = config['iNEXT']['q']),

        AsyEst = expand('out/{data}/{primer}/inext/{filt}-{q}/AsyEst.tsv',
                        data = config['datasets']['data'],  
                        primer = config['datasets']['primer'],
                        filt = config['datasets']['filter']['both'],
                        q = config['iNEXT']['q']),
        
        Graph = expand('out/{data}/{primer}/inext/{filt}-{q}/',
                       data = config['datasets']['data'],  
                       primer = config['datasets']['primer'],
                       filt = config['datasets']['filter']['both'],
                       q = config['iNEXT']['q'])

####################
rule iNEXT:
    #Aim:
    #Use: "iNEXT" and "ggplot2" R packages
    params:
        mirror = config['r']['mirror'],
        datatype = config['iNEXT']['datatype'],
        size = config['iNEXT']['size'],
        endpoint = config['iNEXT']['endpoint'],
        knots = config['iNEXT']['knots'],
        se = config['iNEXT']['se'],
        conf = config['iNEXT']['conf'],
        nboot = config['iNEXT']['nboot']
    input:
        Abundance = 'out/{data}/{primer}/inext/{filt,.*}Abundances.tsv'
    output:
        DataInfo = 'out/{data}/{primer}/inext/{filt,.*}-{q}/DataInfo.tsv',
        iNextEst = 'out/{data}/{primer}/inext/{filt,.*}-{q}/iNextEst.tsv',
        AsyEst = 'out/{data}/{primer}/inext/{filt,.*}-{q}/AsyEst.tsv',
        Graph = 'out/{data}/{primer}/inext/{filt,.*}-{q}/'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("iNEXT")) {{install.packages("iNEXT", repos="{params.mirror}")}}
            if (!require("ggplot2")) {{install.packages("ggplot2", repos="{params.mirror}")}}
            #
            ### LOAD LIBRARIES
            library(iNEXT)   # Interpolation and extrapolation of Hill number with order q.
            library(ggplot2) # You provide the data, tell 'ggplot2' how to map variables to aesthetics, what graphical primitives to use, and it takes care of the details.
            #
            DATA<-read.delim("{input.Abundance}", dec=".") # Import abundance as table
            out<-iNEXT(
                DATA,
                q={wildcards.q},
                datatype="{params.datatype}",
                size={params.size},
                endpoint={params.endpoint},
                knots={params.knots},
                se={params.se},
                conf={params.conf},
                nboot={params.nboot})
            #
            write.table(out$DataInfo, file="{output.DataInfo}", quote=FALSE, sep='\t')
            write.table(out$iNextEst, file="{output.iNextEst}", quote=FALSE, sep='\t')
            write.table(out$AsyEst, file="{output.AsyEst}", quote=FALSE, sep='\t')
            #
            ggiNEXT(
                out,
                type=1,
                se=TRUE,
                facet.var="none",
                color.var="site",
                grey=FALSE)
            #
            ggsave(
                "Rarefaction_iNEXT_Curves.png",
                scale = 3,
                path = "{output.Graph}")
            #
            ggsave(
                "Rarefaction_iNEXT_Curves.pdf",
                scale = 3,
                path = "{output.Graph}")
        """)

####################
rule iNEXT_Convert:
    #Aim: Convert BIOM file into abundance table
    #Use: sed / cut
    input:
        Biom = 'out/{data}/{primer}/qiime2/export/core/{filt,.*}Table/table-from-biom.tsv'
    output:
        Abundances = 'out/{data}/{primer}/inext/{filt,.*}Abundances.tsv'
    shell:
         'sed \'1d\' {input.Biom} | '
         'cut -f2- '
         '> {output.Abundances}'
