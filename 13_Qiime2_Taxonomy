####################
configfile: "config.yaml"

####################
def get_fwd_primer(wildcards):
     return  config['taxonomy'][wildcards]['forward']

def get_rev_primer(wildcards):
     return  config['taxonomy'][wildcards]['reverse']

def get_amplicon_len(wildcards):
     return  config['taxonomy'][wildcards]['lenght']

####################
rule all:
    input:
        TaxoView = expand('out/{data}/{primer}/qiime2/visual/{filt}TaxoView-{ribodatabase}.qzv',
                          data = config['datasets']['data'],
                          primer = config['datasets']['primer'],
                          filt = config['datasets']['filter']['both'],
                          ribodatabase = config['taxonomy']['ribodatabase']),
        
        TaxoBarplot = expand('out/{data}/{primer}/qiime2/visual/{filt}TaxoBar-{ribodatabase}.qzv',
                             data = config['datasets']['data'],
                             primer = config['datasets']['primer'],
                             filt = config['datasets']['filter']['both'],
                             ribodatabase = config['taxonomy']['ribodatabase'])

####################
rule Taxo_Barplot:
    #Aim: This visualizer produces an interactive barplot visualization of taxonomies.
    # Interactive features include multi-level sorting, plot recoloring, category selection/highlighting,
    # sample relabeling, and SVG figure export.
    #Use: qiime taxa barplot [OPTIONS]
    params:
        metadata = config['datasets']['metadata']
    input:
        Table = 'out/{data}/{primer}/qiime2/core/{filt,.*}Table.qza',
        TaxonomySequence = 'out/{data}/{primer}/qiime2/taxonomy/{filt,.*}TaxoRepSeq-{ribodatabase}.qza'
    output:
        Visualization = 'out/{data}/{primer}/qiime2/visual/{filt,.*}TaxoBar-{ribodatabase}.qzv'
    shell:
        'qiime taxa barplot '
        '--i-table {input.Table} '
        '--i-taxonomy {input.TaxonomySequence} '
        '--m-metadata-file {params.metadata} '
        '--o-visualization {output.Visualization}'

####################
rule Taxo_View:
    #Aim: Generate a tabular view of Metadata.
    # The output visualization supports interactive filtering, sorting, and exporting to common file formats.
    #Use: qiime metadata tabulate [OPTIONS]
    input:
        TaxonomySequence = 'out/{data}/{primer}/qiime2/taxonomy/{filt,.*}TaxoRepSeq-{ribodatabase}.qza'
    output:
        Visualization = 'out/{data}/{primer}/qiime2/visual/{filt,.*}TaxoView-{ribodatabase}.qzv'
    shell:
        'qiime metadata tabulate '
        '--m-input-file {input.TaxonomySequence} '
        '--o-visualization {output.Visualization}'

####################
rule Taxonomic_Composition:
    #Aim: Classify reads by taxon using a fitted classifier.
    #Use: qiime feature-classifier classify-sklearn [OPTIONS]
    params:
        threads = config['cluster']['threads'],
        batchsize = config['taxonomy']['batchsize']
    input:
        Classifier = 'out/TAXONOMY/{primer}/Classifier-{ribodatabase}.qza',
        Sequence = 'out/{data}/{primer}/qiime2/core/{filt,.*}RepSeq.qza'
    output:
        TaxonomySequence = 'out/{data}/{primer}/qiime2/taxonomy/{filt,.*}TaxoRepSeq-{ribodatabase}.qza'
    shell:
        'qiime feature-classifier classify-sklearn '
        '--i-classifier {input.Classifier} '
        '--i-reads {input.Sequence} '
        '--p-reads-per-batch {params.batchsize} '
        '--p-n-jobs {params.threads} '
        '--o-classification {output.TaxonomySequence}'

####################
rule Train_Classifier:
    #Aim: Create a scikit-learn naive_bayes classifier for reads.
    #Use: qiime feature-classifier fit-classifier-naive-bayes [OPTIONS]
    input:
        RefSequence = 'out/TAXONOMY/{primer}/RefSeq-{ribodatabase}.qza',
        RefTaxonomy = 'out/TAXONOMY/{primer}/RefTaxo-{ribodatabase}.qza'
    output:
        Classifier = 'out/TAXONOMY/{primer}/Classifier-{ribodatabase}.qza'
    shell:
        'qiime feature-classifier fit-classifier-naive-bayes '
         '--i-reference-reads {input.RefSequence} '
         '--i-reference-taxonomy {input.RefTaxonomy} '
         '--o-classifier {output.Classifier}'

####################
rule No_Extract_Reference_Reads:
    #Aim: Rename import ITS2 DataSeq in ITS2 RefSeq for training. !!! FOR ITS2 ONLY !!! See Note !!!
    #Use: cp
    input:
        DataSequence = 'out/TAXONOMY/ITS2/DataSeq-{ribodatabase}.qza'
    output:
        RefSequence = 'out/TAXONOMY/ITS2/RefSeq-{ribodatabase}.qza'
    shell:
        'cp {input.DataSequence} {output.RefSequence}'

# Fungal ITS classifiers trained on the UNITE reference database do NOT benefit from extracting/trimming reads to primer sites.
# We recommend training UNITE classifiers on the full reference sequences.
# Furthermore, we recommend the 'developer' sequences (located within the QIIME-compatible release download),
# because the standard versions of the sequences have already been trimmed to the ITS region,
# excluding portions of flanking rRNA genes that may be present in amplicons generated with standard ITS primers.
        
####################
rule Extract_Reference_Reads:
    #Aim: Extract sequencing-like reads from a reference database. !!! FOR V4 ONLY !!! See Notes !!!
    #Use: qiime feature-classifier extract-reads [OPTIONS]
    params:
        fwd = config['taxonomy']['V4']['forward'],
        rev = config['taxonomy']['V4']['reverse'],
        length = config['taxonomy']['V4']['lenght']
        #fwd = lambda wildcards: get_fwd_primer(wildcards.primer),
        #rev = lambda wildcards: get_rev_primer(wildcards.primer),
        #length = lambda wildcards: get_amplicon_len(wildcards.primer)
    input:
        DataSequence = 'out/TAXONOMY/V4/DataSeq-{ribodatabase}.qza'
    output:
        RefSequence = 'out/TAXONOMY/V4/RefSeq-{ribodatabase}.qza'
    shell:
        'qiime feature-classifier extract-reads '
        '--i-sequences {input.DataSequence} '
        '--p-f-primer {params.fwd} '
        '--p-r-primer {params.rev} '
        #'--p-trunc-len {params.length} '
        '--o-reads {output.RefSequence}'

# The --p-trunc-len parameter should only be used to trim reference sequences if query sequences are trimmed to this same length or shorter.
# Paired-end sequences that successfully join will typically be variable in length. Single-end reads that are not truncated at a specific length may also be variable in length.
# For classification of paired-end reads and untrimmed single-end reads, we recommend training a classifier on sequences that have been extracted at the appropriate primer sites, but are not trimmed.

# The primer sequences used for extracting reads should be the actual DNA-binding (i.e., biological) sequence contained within a primer construct.
# It should NOT contain any non-biological, non-binding sequence, e.g., adapter, linker, or barcode sequences.
# If you are not sure what section of your primer sequences are actual DNA-binding,
# you should consult whoever constructed your sequencing library, your sequencing center, or the original source literature on these primers.
# If your primer sequences are > 30 nt long, they most likely contain some non-biological sequence.
        
####################
rule Import_Sequence:
    #Aim: Import data to create a new QIIME 2 Artifact.
    #Use: qiime tools import [OPTIONS]
    input:
        FastaSequence = 'inp/qiime2/taxonomy/{primer}/Sequence-{ribodatabase}.fasta'
    output:
        DataSequence = 'out/TAXONOMY/{primer}/DataSeq-{ribodatabase}.qza'
    shell:
        'qiime tools import '
        '--type \'FeatureData[Sequence]\' '
        '--input-path {input.FastaSequence} '
        '--output-path {output.DataSequence}'

####################
rule Import_Taxonomy:
    #Aim: Import data to create a new QIIME 2 Artifact.
    #Use: qiime tools import [OPTIONS]
    input:
        Taxonomy = 'inp/qiime2/taxonomy/{primer}/Taxonomy-{ribodatabase}.txt'
    output:
        RefTaxonomy = 'out/TAXONOMY/{primer}/RefTaxo-{ribodatabase}.qza'
    shell:
        'qiime tools import '
        '--type \'FeatureData[Taxonomy]\' '
        '--source-format HeaderlessTSVTaxonomyFormat '
        '--input-path {input.Taxonomy} '
        '--output-path {output.RefTaxonomy}'
