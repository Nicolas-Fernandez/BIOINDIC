# BIOINDIC
## PROJET EN COURS
Le projet BIOINDIC, Amélioration des connaissances et des pratiques – indicateurs biologiques, se propose de comparer des écosystèmes restaurés à des écosystèmes « naturels » adjacents sur substrats ultramafiques. Cette comparaison sera réalisée au travers de divers grands 
Les objectifs principaux de cette étude sont donc de :
### (i)
caractériser les diversités génétique, taxonomique et fonctionnelle des bactéries, champignons et plantes de zones restaurées et d’écosystèmes « naturels » dans un paysage donné
### (ii)
déterminer les processus écologiques à l’origine de la structure observée (e.g. connectivité fonctionnelle)
### (iii)
mettre en place des indicateurs biologiques du statut des écosystèmes restaurés.
## Durée :
36 mois [2015 -2018]
## Consortium :
IAC – UNC – CIRAD – UNIV. MONTPELLIER
## DOCUMENT
### Télécharger la fiche résumé du projet
http://www.cnrt.nc/wp-content/uploads/2016/03/CNRT_RESUME-BIOINDIC_PS13.pdf
### Télécharger la présentation de la restitution d’avancement des deux projets du 30/09/2016
http://www.cnrt.nc/wp-content/uploads/2016/03/CNRT-restitution-webvc-Bioindic-Recosynth_avancementAnn%C3%A9e129.09.2016.pdf
### Télécharger la présentation de la restitution d’avancement des deux projets du 18/12/2015
http://www.cnrt.nc/wp-content/uploads/2016/03/18122015w-complet-CNRT-R%C3%A9union-av-Proj-RECOSYNTH_BIOINDIC.pdf
## PRESSE
### Télécharger l’article LNC « 35 ans de revégétalisation » écrit par Esther CUNEO
http://www.cnrt.nc/wp-content/uploads/2016/11/201016-Bilan-de-35-ans-de-rev%C3%A9g%C3%A9talisation-recosynth-et-Bioindic-oct2016.pdf
